#!/bin/sh

echo "Using image from tedk42/webjekyll-base:latest"
[[ ! -z "$USER" ]] && USER=$(whoami) && echo "Current user is $USER"
docker build -t $USER/webjekyll-base .

#docker run -v $PWD/website:/data $USER/webjekyll-base bundle exec jekyll build --lsi -s /data

docker run -i -t -p 4000:4000 -v $PWD/website:/data $USER/webjekyll-base \
bundle exec jekyll serve -s /data --watch --force_polling --lsi --host 0.0.0.0
