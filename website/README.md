#Overview
=========================

## Based on
[Antonio Trento](https://it.linkedin.com/in/antoniotrento) jekynewage theme here](https://jekynewage.github.io/)

This Jekyll template was created to develop, landing pages, squeeze pages, portfolio and blog or all the above.

###I integrated analytical tools and marketing such as:
- Google Ad Words
- Google analytics
- Disqus comment system
- Add This social sharing

>>External stylesheets and libraries included are Google Fonts, Font Awesome, Normalize.CSS, and WOW.js

In order to set your log data to applications _config.yml just open the file and find the associated items.

I also built a system to add their own names on the same company files

**To change the base colors yellow go in css folder there main.css where you can set the primary color and the secondary color, remember that the theme is gradient in the background areas**


=========
For more details, read the [documentation](http://jekyllrb.com/)
