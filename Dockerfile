FROM tedk42/webjekyll-base:latest

WORKDIR /data
VOLUME /data
EXPOSE 4000

COPY website/Gemfile .
COPY website/Gemfile.lock .
RUN bundle install
